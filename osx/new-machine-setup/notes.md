
# Sat Oct  5 10:31:26 CEST 2019

## configuring and installing stuff

first need to install xcode command line utilities:

```
xcode-select --install
```

install homebrew:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

This is the new one:
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"


need to add this to shell profiles:

```
eval "$(/opt/homebrew/bin/brew shellenv)"
```




Make sure the doctor is happy (do what it tells you):

```
brew doctor
```

launch brew install script

## kalnarConfig

clone configs

```
cd ~/
mkdir -p config
cd ~/config
git clone https://gitlab.com/kalnarobert/kalnar-mac-config.git
git clone https://gitlab.com/kalnarobert/vimconfig.git vim
git clone https://gitlab.com/kalnarobert/tmuxconfig.git tmux
git clone https://gitlab.com/kalnarobert/bash-config.git bash
git clone https://gitlab.com/kalnarobert/chrome-configs.git chrome
git clone https://gitlab.com/kalnarobert/apple-trackpad-settings.git apple-trachpag
git clone ht	tps://gitlab.com/kalnarobert/rangerconfig.git ranger
git clone https://gitlab.com/kalnarobert/latexconfig.git latex
git clone https://gitlab.com/kalnarobert/gitconfig.git git
```

run scripts

## powerline fonts:

```
wget -c https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf
```

Mac OSX
Open “Font Book” (Hit Cmd + Space to open spotlight and type Font Book).
Go to “File > Add Fonts” and open the PowerlineSymbols.otf file.
After the font is added, right click on “PowerlineSymbols” font and select “Validate Font”.
Now the font should be installed and ready to use.
If you are using iTerm, change the non-ascii font to PowerSymbols to start using the powerline fonts.


## bash

after updating bash, set it to default shell:

bash -v (bash-3.2)
brew install bash
if you close the terminal or open a new tab it will show 4.4 but this still isn't the default version.
  which bash will show you what bash you're using.

```
sudo vi /etc/shells
add the path /usr/local/bin/bash
comment out the others

Change to the new shell
chsh -s /usr/local/bin/bash 
```

## snap

install snap from apple store:

https://apps.apple.com/us/app/snap/id418073146?mt=12

## karabiner

after karabiner is installed, symlink its config file

launch karabiner elements and swap escape and caps lock 

### iterm2

```
brew cask install iterm2
```


super+backspace to delete word:
keys: command+backspace - send hex code 0x15

### trackpad

#### 3-finger drag

Enabling the three finger "drag" is a bit different on El-Capitan and above:

   - Go to System Preferences > Accessibility
   - Choose Mouse & Trackpad from the left options list
   - Click Trackpad Options
   - Tick "enable dragging" and select “three finger drag” from the drop-down menu next to it

## tools

karabiner

### easy move size

https://github.com/dmarcotte/easy-move-resize/issues/1

start on login:
Workaround: Until this is implemented, you can enable launching Easy Move+Resize
at login in System Preferences -> Users & Groups by adding it to the Login Items
tab for your user

# hot corners

settings > desktop & screen savers > 

	- bottom left to application windows
	- top right to mission control

# iterm / terminal

## terminal

meta key:

![Screenshot 2022-04-08 at 22.32.06](./terminal_alt_key.png)

set up font to powerline symbols:

![Screenshot 2022-04-09 at 09.27.38](terminal_font.png)

## iterm

![Screenshot 2022-04-08 at 22.35.53](iterm_alt_key.png)

# screenshots

to set up screenshots to be saved as jpg (reduced size):

```
defaults write com.apple.screencapture type jpg
```



## shortcuts

![](shortcut_mission_control.png)



![image-20221209165746901](change_keyboard_layout_shortcut.png)



# Bash completion for git



````sh
brew install bash-completion@2
````

then add this to `.bash_profile`

```shell
# collection of command line command completions (needed for git completions for example) [[ -r "/opt/homebrew/etc/profile.d/bash_completion.sh" ]] && . "/opt/homebrew/etc/profile.d/bash_completion.sh"
```



# gradle-completion



```
brew install gradle-completion
```



# todo



	- set up gradle bash completion
	- set up Java
	- fix fzf for history in bash bindings

