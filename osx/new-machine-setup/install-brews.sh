#!/bin/bash

brew install bash
brew install vim
brew install tmux
brew install git
brew install wget
brew install ranger
brew install coreutils
brew install iterm2
brew install tree

brew install karabiner-elements 

# java
brew install java 

# android
brew install ant
brew install maven
brew install gradle
brew install android-sdk
brew install android-ndk

# prompt to accept license
android update sdk --no-ui

brew install android-studio


# Update your environment variables:
# 
# export ANT_HOME=/usr/local/opt/ant
# export MAVEN_HOME=/usr/local/opt/maven
# export GRADLE_HOME=/usr/local/opt/gradle
# export ANDROID_HOME=/usr/local/opt/android-sdk
# export ANDROID_SDK_ROOT="/usr/local/share/android-sdk"
# export ANDROID_NDK_HOME=/usr/local/opt/android-ndk
# export ANDROID_NDK_HOME="/usr/local/share/android-ndk"
# Update your paths (bonus points to a better solution to the hardcoded build tools version):
# 
# export PATH=$ANT_HOME/bin:$PATH
# export PATH=$MAVEN_HOME/bin:$PATH
# export PATH=$GRADLE_HOME/bin:$PATH
# export PATH=$ANDROID_HOME/tools:$PATH
# export PATH=$ANDROID_HOME/platform-tools:$PATH
# export PATH=$ANDROID_HOME/build-tools/19.1.0:$PATH


# node and nvm
# todo: not the right way to install
# brew install nvm 
# nvm install stable

# sdk manager

curl -s "https://get.sdkman.io" | bash

# slack
brew cask install slack 

# karabiner for keyboard customization


# version control

# brew cask install smartgit 


# multimedia
brew install vlc 


brew install eclipse-java
brew install postman

# brew cask install firefox
brew install intellij-idea
brew install docker

# command+drag to move/resize
brew install easy-move-plus-resize

# diverse tools

# clipboard management
brew install clipy

# needed for vim tagbar
brew install ctags

# youtube dl
brew install youtube-dl

# gifify
brew install gifify

# graphviz
brew install graphviz

# choose-gui (fuzzy matcher of osx that uses std{id,out} and a native gui; similar to rofi)
brew install choose-gui

# script keybinder with LUA
#brew cask install hammerspoon

# dex2jar for decompiling apks
brew install dex2jar

#brew tap colinsteain/imgcat
#brew install imgcat

# fzf
brew install fzf

# bash
brew install bash

# tmux
brew install tmux

brew install --cask smartgit

brew install --cask goldendict

brew install --cask keepassxc

brew install rename

brew install findutils

brew install --cask nordvpn

brew install avg-antivirus

brew install android-file-transfer

brew install bash-completion

# after install:
# Add the following line to your ~/.bash_profile:
# [[ -r "/opt/homebrew/etc/profile.d/bash_completion.sh" ]] && . "/opt/homebrew/etc/profile.d/bash_completion.sh"

brew install gradle-completion

brew install --cask google-cloud-sdk

brew install scrcpy
brew install zoom
brew install qrencode
brew install --cask db-browser-for-sqlite
brew install pdftk-java
brew install --cask libreoffice
brew install --cask typora
brew install brave-browser
brew install npm
brew install bash-completion@2
brew install gradle-completion
brew install kdoctor
brew install ruby
brew install z
brew install microsoft-teams
brew install jetbrains-toolbox
brew install zbar

